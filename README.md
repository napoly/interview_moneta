# Moneta interview

Simple Spring Boot example to transform text.

##Requirements
For building and running the application you should have:

JDK 11

Gradle 6.7.1

##Running the application locally
There are several ways to run a Spring Boot application on your local machine. One way is to execute the main method 
in the cz.moneta.interview.InterviewApplication class from your IDE.

There is also a SoupUI project in resources which can be used to test the functionality of the REST endpoint.