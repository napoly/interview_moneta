package cz.moneta.interview;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class InterviewApplicationIT {

	@Autowired
	private MockMvc mvc;

	@Test
	void testTransformEndpoint() throws Exception {
		mvc.perform(post("/transform").content("lorem ipsum")).andExpect(status().isOk());
	}

}
