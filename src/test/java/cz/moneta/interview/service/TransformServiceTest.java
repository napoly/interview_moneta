package cz.moneta.interview.service;

import cz.moneta.interview.serviceImpl.TransformService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransformServiceTest {

    TransformService transformService;

    @Test
    public void testTransformation() {
        transformService = new TransformService();
        String result = transformService.transformText("Ahoj, jak se máš?");
        assertEquals("?šÁm Es kAj ,jOha", result);
    }
}
