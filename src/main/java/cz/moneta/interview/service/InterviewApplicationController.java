package cz.moneta.interview.service;

import cz.moneta.interview.serviceApi.ITransformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InterviewApplicationController {

    @Autowired
    private ITransformService transformService;

    @PostMapping(value = "/transform")
    String transformText(@RequestBody String textToTransform) {
        return transformService.transformText(textToTransform);
    }
}
