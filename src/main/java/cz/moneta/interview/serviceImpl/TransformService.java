package cz.moneta.interview.serviceImpl;

import cz.moneta.interview.serviceApi.ITransformService;
import org.springframework.stereotype.Component;

import static java.lang.Character.toLowerCase;
import static java.lang.Character.toUpperCase;

@Component
public class TransformService implements ITransformService {

    @Override
    public String transformText(String text) {
        String transformedText = new StringBuilder(text).reverse().toString()
                .trim().replaceAll(" +", " ");
        return uppercaseVowels(transformedText);
    }

    private String uppercaseVowels(String text) {
        char[] str = text.toCharArray();

        for (int i = 0; i < str.length; i++) {
            char c = str[i];
            if (c == 'a' || c == 'á' || c == 'e' || c == 'é' || c == 'ě' || c == 'i' || c == 'í' || c == 'o' || c == 'ó'
                    || c == 'u' || c == 'ú' || c == 'ů') {
                str[i] = toUpperCase(c);
            } else {
                str[i] = toLowerCase(c);
            }
        }
        return new String(str);
    }
}
