package cz.moneta.interview.serviceApi;

public interface ITransformService {

    /**
     * Returns reverted text with reverse upper/lowercase vowels and trimmed whitespace
     * @param text String for transformation
     * @return {@link String}
     */
    String transformText(String text);

}
